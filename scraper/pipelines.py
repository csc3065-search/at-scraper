# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import logging
import os
from typing import Union

import scrapy
from pymongo import MongoClient
from scrapy.pipelines.images import ImagesPipeline

from scraper.items import Vehicle
from scraper.spiders.autotrader_spider import AutotraderSpider

MONGODB_URI = os.environ['MONGODB_URI']
MONGODB_DATABASE_NAME = os.environ['MONGODB_DATABASE']
MONGODB_COLLECTION_NAME = 'vehicles'


class AutotraderPipeline(object):
    def __init__(self):
        self.database = MongoClient(MONGODB_URI).get_database(MONGODB_DATABASE_NAME)
        self.collection = self.database.get_collection('vehicles')

    def process_item(self, item: Union[Vehicle], spider: AutotraderSpider):
        if isinstance(item, Vehicle):
            self.process_vehicle(item)

    def process_vehicle(self, item: Vehicle):
        id_ = item[Vehicle.ID]
        existing = self.collection.find_one({'_id': id_})
        if not existing:
            self.collection.insert(item.to_db_dict())
            logging.info(f'Recorded a new vehicle: {id_}')


class ImagesWithNamesPipeline(ImagesPipeline):
    IMAGE_NAME = 'image_name'

    def get_media_requests(self, item, info):
        new_urls_field = []
        for idx, image_url in enumerate(item[self.IMAGES_URLS_FIELD]):
            vehicle_id = item[Vehicle.ID]
            image_extension = image_url.split('.')[-1]
            image_name = f'full/{vehicle_id}/{idx}.{image_extension}'
            yield scrapy.Request(url=image_url, meta={ImagesWithNamesPipeline.IMAGE_NAME: image_name}, priority=10)
            new_urls_field.append(image_name.replace('full', '{size}'))
        item[self.IMAGES_URLS_FIELD] = new_urls_field

    def file_path(self, request, response=None, info=None):
        return request.meta[ImagesWithNamesPipeline.IMAGE_NAME]

    def thumb_path(self, request, thumb_id, response=None, info=None):
        return f'{request.meta[ImagesWithNamesPipeline.IMAGE_NAME].replace("full", thumb_id)}'
