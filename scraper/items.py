import scrapy
from scrapy import Item


class Vehicle(Item):
    ID = 'id'
    TITLE = 'title'
    SLUG = 'slug'
    DESCRIPTION = 'description'
    KEYWORDS = 'keywords'
    PRICE = 'price'
    IMAGE_URLS = 'image_urls'
    API_DATA = 'api_data'

    id = scrapy.Field()
    title = scrapy.Field()
    slug = scrapy.Field()
    description = scrapy.Field()
    keywords = scrapy.Field()
    price = scrapy.Field()
    image_urls = scrapy.Field()
    api_data = scrapy.Field()

    def to_db_dict(self) -> dict:
        return {
            '_id': self[Vehicle.ID],
            Vehicle.TITLE: self[Vehicle.TITLE],
            Vehicle.SLUG: self[Vehicle.SLUG],
            Vehicle.DESCRIPTION: self[Vehicle.DESCRIPTION],
            Vehicle.KEYWORDS: self[Vehicle.KEYWORDS],
            Vehicle.PRICE: self[Vehicle.PRICE],
            Vehicle.IMAGE_URLS: self[Vehicle.IMAGE_URLS],
            Vehicle.API_DATA: self[Vehicle.API_DATA]
        }
