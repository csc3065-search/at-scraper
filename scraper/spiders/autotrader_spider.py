#!/usr/bin/python3
import json
import logging
import re

from scrapy import Spider, Request
from scrapy.exceptions import CloseSpider
from scrapy.http import Response

from scraper.items import Vehicle

ITEM = 'item'

START_URL = 'https://www.autotrader.co.uk/car-search?search-target=usedcars&postcode=BT11AA&page=1'


# noinspection PyMethodMayBeStatic
class AutotraderSpider(Spider):
    name = 'autotrader'

    def start_requests(self):
        yield Request(START_URL, callback=self.parse_listing)

    def parse_listing(self, response: Response):
        logging.info(f'Parsing page \'{response.url}\'')
        listings = response.xpath('//article[contains(@class, \'search-listing\')]')

        over_limit = response.xpath('//p[@class=\'search-page__pagination-over-limit-message\']').get()
        next_page = re.sub(r'page=(.*)$', 'page=' + response.xpath('//a[@class=\'pagination--right__active\'][1]/@data-paginate').get(), response.url)
        if over_limit:
            raise CloseSpider('Scraped all available pages (limit hit)')

        for listing in listings:
            vehicle = Vehicle()

            title = listing.xpath('.//h2[contains(@class, \'listing-title\')]/a/text()').get()
            logging.info(f'Found a vehicle listing: \'{title}\'')

            vehicle[Vehicle.SLUG] = listing.xpath('.//h2[contains(@class, \'listing-title\')]/a/@href').get().split('?')[0][1:]
            vehicle[Vehicle.ID] = vehicle[Vehicle.SLUG].split('/')[-1]
            vehicle[Vehicle.TITLE] = title
            vehicle[Vehicle.KEYWORDS] = [t.get() for t in listing.xpath('.//ul[@class=\'listing-key-specs \']/li/text()')]
            vehicle[Vehicle.PRICE] = listing.xpath('.//div[@class=\'vehicle-price\']/text()').get()

            yield response.follow(f'https://www.autotrader.co.uk/json/fpa/initial/{vehicle[Vehicle.ID]}', callback=self.parse_vehicle_api_data, meta={'item': vehicle}, priority=5)
        yield response.follow(next_page, self.parse_listing, priority=-5)

    def parse_vehicle_api_data(self, response: Response):
        vehicle = response.meta[ITEM]
        api_response_json = json.loads(response.body.decode("utf-8"))

        vehicle[Vehicle.TITLE] = api_response_json['advert']['title']
        vehicle[Vehicle.DESCRIPTION] = api_response_json['advert']['description']
        vehicle[Vehicle.API_DATA] = api_response_json
        vehicle[Vehicle.IMAGE_URLS] = [u.replace('{resize}/', '') for u in api_response_json['advert']['imageUrls']]
        yield vehicle
