FROM python:3.7-alpine as base
FROM base as builder

COPY requirements.txt /requirements.txt
RUN apk --update add libffi-dev openssl-dev python-dev build-base libxslt-dev jpeg-dev zlib-dev && pip3 install -r /requirements.txt

COPY scraper /app/scraper
COPY scrapy.cfg /app
WORKDIR /app

CMD ["scrapy", "crawl", "autotrader"]
